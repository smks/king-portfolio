# King Portfolio Test
### Created by Shaun Michael K. Stone (smks)

Games Library with Favourites

[Mobile](screenshots/mobile.jpg)
[Desktop](screenshots/desktop.png)

## How to Run

* You'll need to have [git](https://git-scm.com/) and [node](https://nodejs.org/en/) (version 6.10 or higher) installed in your system.

* Install the dependencies in the root of project:

```
npm install
```

* Run development server:

```
npm start
```

Open the web browser to `http://localhost:8888/`

### To build the production package

```
npm run build
```

This will create a `public` directory. You can open the index.html in this directory to interact with the production release.

### How does it work?

1. By default you will be taking to all games.
2. Click on each of your favourite games to add them to your favourites.
3. At the top of the screen, choose the favourites tab.
4. These are all the games you've added. If you click on one of them it will show a detail modal with a play now button.
5. If you click the play now button, you will be taken to the game page on the king website.
6. If you click the favourited game again in the 'all games' mode, then it will be removed from your favourites.

### Test Demonstrations

* Mobile Responsive friendly.
* ES6 features such as arrow functions, classes, let & const, spread operator, template strings & destructuring.
* Use of Event Emitter to avoid deep nested callbacks (Favourites counter).
* Use of Flexbox to manage game listing and allow for responsive design.
* SVG use Logo to reduce download size.
* By default use the smallest sized images to load page quicker.
* Load only 20 games at first to avoid excessive image download requests.
* Follows ESLint for consistent coding standards.
* Persistence applied via local storage.
* Minification and bundling of JavaScript to prevent multiple network requests.
* Autoprefixer Postcss applied to cater for all browser vendors

### Eslint

```
npm run lint
```
