import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import KingApp from './jsx/app.jsx';

render(<AppContainer><KingApp /></AppContainer>, document.querySelector("#app"));

if (module && module.hot) {
  module.hot.accept('./jsx/app.jsx', () => {
    const App = require('./jsx/app.jsx').default;
    render(
      <AppContainer>
        <App/>
      </AppContainer>,
      document.querySelector("#app")
    );
  });
}
