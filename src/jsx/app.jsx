import React from 'react';
import Navigation from './components/Navigation';
import Header from './components/Header';
import Lobby from './screens/Lobby';
import MODES from './constants/modes';

import '../../styles/index.scss';

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      imageSizes: '60X60PX',
      currentMode: MODES.ALL_GAMES
    };
    this.onModeChange = this.onModeChange.bind(this);
    this.changeImages = this.changeImages.bind(this);
  }

  onModeChange(link) {
    this.setState({
      currentMode: link
    });
  }

  changeImages(imageSizes) {
    this.setState({imageSizes});
  }

  render() {
    const {imageSizes, currentMode} = this.state;
    let activeScreen = <Lobby imageSizes={imageSizes} mode={currentMode} />

    return (
      <div>
        <Header />
        <Navigation 
          activeLink={this.state.currentMode} 
          onNavigationLinkClicked={this.onModeChange} 
          onImageSizeChange={this.changeImages} />
        {activeScreen}
      </div>
    )
  }
}
