import React from 'react';
import PropTypes from 'prop-types';
import GameTile from './GameTile';

class Card extends React.Component {

  constructor(props) {
    super(props);
    this.data = {
      isFavourited: false
    };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.onCardPressed({
      fullName: this.props.gameName,
      name: this.props.gameShortName,
      url: this.props.url
    });
  }

  render() {  
    const gameTile = <GameTile
            name={this.props.gameName}
            imageSizes={this.props.imageSizes}
            short={this.props.gameShortName} />;
        
    const activeClass = this.props.isActive ? 'is-active' : '';

    return <div className={`card ${activeClass}`} onClick={this.onClick} data-url={this.props.url}>
            <div className="card-title">
                <h1>{this.props.gameName}</h1>
            </div>
            <div className="card-body">
                {gameTile}
                <div className="heart" />
            </div>
        </div>;
  }
}

Card.propTypes = {
  onCardPressed: PropTypes.func,
  gameName: PropTypes.string,
  gameShortName: PropTypes.string,
  url: PropTypes.string,
  imageSizes: PropTypes.string,
  isActive: PropTypes.bool
};

export default Card;
