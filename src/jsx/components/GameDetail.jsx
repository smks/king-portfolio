import React from 'react';
import PropTypes from 'prop-types';
import gameUrls from '../constants/game-urls';

class GameDetail extends React.Component {

  constructor(props) {
    super(props);
    this.onPlayNow = this.onPlayNow.bind(this);
  }

  onPlayNow() {
    const {url} = this.props;
    window.location = `http://royal1.midasplayer.com${url}`;
  }

  render() {
    const imageUrl = gameUrls.ICON_764X260PX.replace(/\[game_shortname\]/gi, this.props.name);
    const screenShotUrl = gameUrls.SCREENSHOTS.replace(/\[game_shortname\]/gi, this.props.name);

    return <div className="game-detail">
            <div className="underlay" />
            <div className="game-card">
                <a href="#" className="close-button" onClick={this.props.closeGame}></a>
                <img className="game-header" src={`${imageUrl}`} alt={`${this.props.name}`}/>
                <p>Lorem ipsum dolor sit amet, ei meis minimum imperdiet sea, graeci quodsi no vim. Ad quaeque appetere liberavisse pro. Eu sea sonet iudicabit. Et vis munere offendit, te mazim repudiare scripserit vel, dolor electram cu usu. Ei aliquam ponderum ius, dolor imperdiet eos ut, in aliquid inimicus consetetur eum.</p>
                <button className="play-now-button" onClick={this.onPlayNow}>Play Now</button>
                <img className="screenshot" src={`${screenShotUrl}`} alt={`${this.props.name}`}/>
            </div>
        </div>;
  }
}

GameDetail.propTypes = {
  name: PropTypes.string,
  url: PropTypes.string,
  closeGame: PropTypes.func
};

export default GameDetail;
