import React from 'react';
import PropTypes from 'prop-types';
import gameUrls from '../constants/game-urls';

class GameTile extends React.Component {

  constructor(props) {
    super(props);
    this.name = props.name;
    this.shortName = props.short;
  }

  render() {
    const imageUrl = gameUrls[`ICON_${this.props.imageSizes}`].replace(/\[game_shortname\]/gi, this.shortName);
        
    return <div className={`game-tile game-tile-${this.shortName}`}>
            <img src={`${imageUrl}`} alt={`${this.name}`}/>
        </div>;
  }
}

GameTile.propTypes = {
  name: PropTypes.string,
  short: PropTypes.string,
  imageSizes: PropTypes.string
};

export default GameTile;
