import React from 'react';
import kingLogo from '../../images/king-logo.svg';

export default class Header extends React.Component {
  render() {        
    return <header><img className="king-logo" src={kingLogo} alt="King"/></header>;
  }
}
