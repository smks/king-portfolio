import React from 'react';
import PropTypes from 'prop-types';
import storage from '../storage/storage';
import favouritesCountEvent from '../events/favouritesCountEvent';

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      navLinks: ['All Games', 'Favourites'],
      favouritesCount: storage.getFavourites().length
    };
    this.onImageSizeChange = this.onImageSizeChange.bind(this);
    this.onChangeMode = this.onChangeMode.bind(this);
    favouritesCountEvent.on('favourites.count', (favouritesCount) => {
      this.setState({favouritesCount});
    });
  }

  render() {
    const navigation = this.state.navLinks.map((link, index) => {
      const isActive = this.props.activeLink === (index + 1);
      return <li data-id={index + 1} key={`nav-${link}`} className={isActive ? 'is-active' : ''} onClick={this.onChangeMode}>
                <a href="#">{link}</a>
            </li>;
    });

    const {favouritesCount} = this.state;

    return <nav>
            <ul className="navigation">
                {navigation}
                <li className="favourites-counter"><a href="#">{favouritesCount}</a></li>
                <li className="image-size-changer">
                    <select defaultValue="60X60PX" onChange={this.onImageSizeChange}>
                        <option>60X60PX</option>
                        <option>81X46PX</option>
                        <option>170X80PX</option>
                        <option>764X260PX</option>
                    </select>
                </li>
            </ul>
        </nav>;
  }

  onImageSizeChange(e) {
    this.props.onImageSizeChange(e.currentTarget.value);
  }

  onChangeMode(e) {
    e.preventDefault();
    this.props.onNavigationLinkClicked(parseInt(e.currentTarget.getAttribute('data-id'), 10));
  }
}

Navigation.propTypes = {
  activeLink: PropTypes.number,
  onImageSizeChange: PropTypes.func,
  onNavigationLinkClicked: PropTypes.func
};

export default Navigation;
