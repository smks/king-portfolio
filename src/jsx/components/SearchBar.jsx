import React from 'react';
import PropTypes from 'prop-types';

class SearchBar extends React.Component {
  render() { 
    return <div className="search-bar">
      <input type="text" placeholder="Search Games" onKeyUp={this.props.onKeyUp}/>
    </div>;
  }
}

SearchBar.propTypes = {
  onKeyUp: PropTypes.func
};

export default SearchBar;
