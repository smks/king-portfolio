const GAME_URLS = {
  SCREENSHOTS: 'http://royal1.midasplayer.com/images/games/[game_shortname]/dumps/screen_[game_shortname].gif',
  ICON_60X60PX: 'http://royal1.midasplayer.com/images/games/[game_shortname]/[game_shortname]_60x60.gif',
  ICON_81X46PX: 'http://royal1.midasplayer.com/images/games/[game_shortname]/[game_shortname]_81x46.gif',
  ICON_170X80PX: 'http://royal1.midasplayer.com/images/games/[game_shortname]/[game_shortname]_170x80.gif',
  ICON_764X260PX: 'http://royal1.midasplayer.com/images/games/[game_shortname]/tournamentPage/[game_shortname]_764x260.jpg'
};

export default GAME_URLS;
