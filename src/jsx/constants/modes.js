const MODES = {
  ALL_GAMES: 1,
  MY_GAMES: 2
};

export default MODES;
