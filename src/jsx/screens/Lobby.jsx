import React from 'react';
import PropTypes from 'prop-types';
import Card from '../components/Card';
import GameDetail from '../components/GameDetail';
import SearchBar from '../components/SearchBar';
import gamesList from '../../resources/games.json';
import MODES from '../constants/modes';
import storage from '../storage/storage';
import favouritesCountEvent from '../events/favouritesCountEvent';

class Lobby extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gamesToShow: 20,
      favouriteGames: storage.getFavourites(),
      searchString: '',
      gameDetail: null
    };
    this.data = {
      totalGames: gamesList.games.length,
      gamesIncrement: 20,
      hasReachedMaxGameCount: false,
      gamesList,
      gamesListVisible: []
    };
    this.keyUpWait = null;
    this.onLoadMoreGames = this.onLoadMoreGames.bind(this);
    this.onKeyUpSearch = this.onKeyUpSearch.bind(this);
    this.onCardPressed = this.onCardPressed.bind(this);
    this.openGame = this.openGame.bind(this);
    this.onCloseGameDetail = this.onCloseGameDetail.bind(this);
  }

  onLoadMoreGames() {
    let newGameCount = this.state.gamesToShow + this.data.gamesIncrement;
    if (newGameCount >= this.data.totalGames) {
      this.data.hasReachedMaxGameCount = true;
      newGameCount = this.data.totalGames;
    }
    this.setState({gamesToShow: newGameCount});
  }

  onCardPressed({name}) {
    let {favouriteGames} = this.state;
    const hasGameAsFavourite = favouriteGames.includes(name);
    if (!hasGameAsFavourite) {
      favouriteGames.push(name);
    } else if (hasGameAsFavourite) {
      favouriteGames.splice(favouriteGames.indexOf(name), 1);
    }
    storage.saveFavourites(favouriteGames);
    favouritesCountEvent.emit('favourites.count', favouriteGames.length);
    this.setState({favouriteGames});
  }

  onKeyUpSearch(e) {
    const searchString = e.currentTarget.value;
    clearTimeout(this.keyUpWait);
    this.keyUpWait = setTimeout(() => {
      this.setState({searchString});
    }, 200);
  }

  getMyGames() {
    const {games} = gamesList;
    const {favouriteGames} = this.state;
    let gamesListToShow = games.filter(game => favouriteGames.includes(game.short));

    if (this.state.searchString !== '') {
      const {searchString} = this.state;
      gamesListToShow = gamesListToShow.filter(game => game.name.toLowerCase()
        .match(new RegExp(searchString.toLowerCase())));
    }

    return gamesListToShow.map((game) => {
      const isActive = this.state.favouriteGames.includes(game.short);
      return <Card 
        gameName={game.name} 
        gameShortName={game.short}
        url={game.url}
        key={game.short} 
        isActive={isActive} 
        onCardPressed={this.openGame} 
        imageSizes={this.props.imageSizes} />;
    });
  }

  openGame(game) {
    scroll(0, 0);
    this.setState({
      gameDetail: game
    });
  }

  onCloseGameDetail() {
    this.setState({
      gameDetail: null
    });
  }

  getAllGames() {
    const {games} = gamesList;
    const {gamesToShow, searchString} = this.state;

    if (searchString !== '') {
      this.data.gamesListToShow = games.filter(game => game.name.toLowerCase().match(new RegExp(searchString.toLowerCase())));
    } else {
      this.data.gamesListToShow = games.slice(0, gamesToShow);
    }
    
    const {gamesListToShow} = this.data;

    return gamesListToShow.map((game) => {
      const isActive = this.state.favouriteGames.includes(game.short);
      return <Card 
        gameName={game.name} 
        gameShortName={game.short} 
        key={game.short} 
        isActive={isActive} 
        onCardPressed={this.onCardPressed} 
        imageSizes={this.props.imageSizes} />;
    });
  }

  getLoadMoreButton() {
    const {hasReachedMaxGameCount} = this.data;
    return (hasReachedMaxGameCount === false && 
      this.state.searchString === '' &&
      this.props.mode !== MODES.MY_GAMES) ? <button className="button load-more" onClick={this.onLoadMoreGames}>Load More</button> : '';
  }

  getCards() {
    let cards;
    
    switch (this.props.mode) {
    case MODES.MY_GAMES:
      cards = this.getMyGames();
      break;
    default:
      cards = this.getAllGames();
    }

    return cards;
  }

  getGameDetail() {
    let gameDetail;
    if (this.state.gameDetail) {
      gameDetail = <GameDetail {...this.state.gameDetail} closeGame={this.onCloseGameDetail}/>;
    }
    return gameDetail;
  }

  render() {
    const loadMoreButton = this.getLoadMoreButton();
    const cards = this.getCards();
    const gameDetail = this.getGameDetail();

    return (
      <div>
        <SearchBar onKeyUp={this.onKeyUpSearch} />
        {gameDetail}
        <section className="games-container">
            {cards}
        </section>;
        {loadMoreButton}
      </div>
    )
  }
}

Lobby.propTypes = {
  mode: PropTypes.number,
  imageSizes: PropTypes.string
};

export default Lobby;
