export default new class Data {
    
  constructor() {
    this.storage = window.localStorage;
  }

  save(key, value) {
    this.storage.setItem(key, value)
  }

  get(key) {
    return this.storage.getItem(key);
  }

  saveFavourites(favourites) {
    this.save('favourites', JSON.stringify(favourites));
  }

  getFavourites() {
    const favourites = this.get('favourites');
    if (favourites === null) {
      return [];
    }
    return JSON.parse(favourites);
  }
}
